import * as assets from './assets'
import * as entries from './entries'

export default {
  assets,
  entries
}
